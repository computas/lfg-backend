this is the project for the lfg-backend

requires
 - mvn 2.2.1 or greater
 - jdk 7

build with:
- mvn clean install
- integration tests: mvn clean install -PrestIt

run tomcat with:
- mvn tomcat7:run-war
- test with http://localhost:8090/lfg-backend/rest/environment