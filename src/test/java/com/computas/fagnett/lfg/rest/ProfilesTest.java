package com.computas.fagnett.lfg.rest;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.TestBase;
import com.computas.fagnett.lfg.model.Profile;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang.StringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

/**
 * Author: Michael Gfeller
 */
@Test(groups = {"integration"})
public class ProfilesTest extends TestBase {
	private WebResource webResource;
	private Profile addedProfile;

	@BeforeClass(alwaysRun = true)
	public void setupWebResource() {
		webResource = client.resource(endpointBaseUrl + Resource.PROFILES_NAME);
	}

	public void testAddProfile() throws IOException {
		Profile profile2Add = new Profile("Ole Nordmann");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, profile2Add);
		Assert.assertEquals(response.getStatus(), ClientResponse.Status.CREATED.getStatusCode());
		addedProfile = response.getEntity(Profile.class);
		Assert.assertNotNull(addedProfile);
		Assert.assertEquals(addedProfile.getName(), profile2Add.getName());
		Assert.assertTrue(StringUtils.isNotBlank(addedProfile.getId()), "addedProfile.getId() must not be blank");
		String location = response.getLocation().toString();
		String expectedLocation = "http://localhost:8090/lfg/rest/profiles/" + addedProfile.getId();
		Assert.assertEquals(location, expectedLocation, "Location");
		Assert.assertTrue(response.getHeaders().containsKey(Resource.CONTENT_LOCATION));
		Assert.assertEquals(response.getHeaders().get(Resource.CONTENT_LOCATION).size(), 1);
		Assert.assertEquals(response.getHeaders().get(Resource.CONTENT_LOCATION).get(0), expectedLocation, Resource.CONTENT_LOCATION);
	}

	@Test(dependsOnMethods = {"testAddProfile"})
	public void testGetProfileById() {
		WebResource getProfileResource = client.resource(endpointBaseUrl + Resource.PROFILES_NAME + "/" + addedProfile.getId());
		ClientResponse response = getProfileResource.get(ClientResponse.class);
		Assert.assertEquals(response.getStatus(), ClientResponse.Status.OK.getStatusCode());
		Profile profile = response.getEntity(Profile.class);
		Assert.assertNotNull(profile);
		Assert.assertEquals(profile.getId(), addedProfile.getId());
		Assert.assertEquals(profile.getName(), addedProfile.getName());
	}

	@Test(dependsOnMethods = {"testGetProfileById"})
	public void testGetProfiles() {
		ClientResponse response = webResource.get(ClientResponse.class);
		Assert.assertEquals(response.getStatus(), ClientResponse.Status.OK.getStatusCode());
		List<Profile> profiles = response.getEntity(new GenericType<List<Profile>>() {
		});
		Assert.assertNotNull(profiles);
		Assert.assertFalse(profiles.isEmpty());
		Profile lastProfile = null;
		boolean foundAddedProfile = false;
		for (Profile profile : profiles) {
			report(profile.toString());
			// assert sort order
			if (lastProfile != null) {
				Assert.assertTrue(lastProfile.getName().compareTo(profile.getName()) <= 0, "sorting error");
			}
			if (addedProfile.getId().equals(profile.getId())) {
				foundAddedProfile = true;
			}
			lastProfile = profile;
		}
		Assert.assertTrue(foundAddedProfile, "Added profile not found: " + addedProfile.toString());
	}

}
