package com.computas.fagnett.lfg.rest;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.TestBase;
import com.computas.fagnett.lfg.model.Profile;
import com.computas.fagnett.lfg.model.Skill;
import com.computas.fagnett.lfg.model.SkillLevel;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Random;

/**
 * Author: Michael Gfeller
 */

//TODO: test failure scenarios, like non-existing profile or skill

@Test(groups = {"integration"})
public class SkillLevelsTest extends TestBase {
	private List<Skill> skills;
	private List<Profile> profiles;
	private SkillLevel addedSkillLevel;

	public void testGetAllSkills() {
		WebResource webResource = client.resource(endpointBaseUrl + Resource.SKILLS_NAME);
		ClientResponse response = webResource.get(ClientResponse.class);
		Assert.assertEquals(response.getStatus(), ClientResponse.Status.OK.getStatusCode());
		skills = response.getEntity(new GenericType<List<Skill>>() {
		});
		Assert.assertNotNull(skills);
		Assert.assertFalse(skills.isEmpty());
	}

	@Test(dependsOnMethods = {"testGetAllSkills"})
	public void testGetAllProfiles() {
		WebResource webResource = client.resource(endpointBaseUrl + Resource.PROFILES_NAME);
		ClientResponse response = webResource.get(ClientResponse.class);
		Assert.assertEquals(response.getStatus(), ClientResponse.Status.OK.getStatusCode());
		profiles = response.getEntity(new GenericType<List<Profile>>() {
		});
		Assert.assertNotNull(profiles);
		Assert.assertFalse(profiles.isEmpty());
	}

	@Test(dependsOnMethods = {"testGetAllProfiles"})
	public void testAddSkillLevel() {
		Random random = new Random(System.currentTimeMillis());
		//given a list with all profiles
		//and a list with all skills
		//and a random level
		final int MAX_LEVEL = 1000;
		int level = random.nextInt(MAX_LEVEL) + 1;
		//select a random profile
		Profile profile = profiles.get(random.nextInt(profiles.size()));
		//select a random skill
		Skill skill = skills.get(random.nextInt(skills.size()));

		//when creating a new skill level
		SkillLevel skillLevel2Add = new SkillLevel(profile.getId(), skill.getId(), level);
		WebResource webResource = client.resource(endpointBaseUrl + Resource.SKILL_LEVELS_NAME);
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, skillLevel2Add);

		//then check the result
		Assert.assertEquals(response.getStatus(), ClientResponse.Status.CREATED.getStatusCode());
		addedSkillLevel = response.getEntity(SkillLevel.class);
		Assert.assertNotNull(addedSkillLevel);
		Assert.assertTrue(StringUtils.isNotBlank(addedSkillLevel.getId()), "addedSkillLevel.getId() must not be blank");
		Assert.assertEquals(addedSkillLevel.getProfileId(), skillLevel2Add.getProfileId());
		Assert.assertEquals(addedSkillLevel.getSkillId(), skillLevel2Add.getSkillId());
		String location = response.getLocation().toString();
		String expectedLocation = "http://localhost:8090/lfg/rest/skilllevels/" + addedSkillLevel.getId();
		Assert.assertEquals(location, expectedLocation, "Location");
		Assert.assertTrue(response.getHeaders().containsKey(Resource.CONTENT_LOCATION));
		Assert.assertEquals(response.getHeaders().get(Resource.CONTENT_LOCATION).size(), 1);
		Assert.assertEquals(response.getHeaders().get(Resource.CONTENT_LOCATION).get(0), expectedLocation, Resource.CONTENT_LOCATION);
	}

	@Test(dependsOnMethods = {"testAddSkillLevel"})
	public void testGetSkillForProfile() {
		WebResource webResource = client.resource(endpointBaseUrl + Resource.SKILL_LEVELS_NAME
				+ "?profile=" + addedSkillLevel.getProfileId());
		ClientResponse response = webResource.get(ClientResponse.class);
		Assert.assertEquals(response.getStatus(), ClientResponse.Status.OK.getStatusCode());
		List<SkillLevel> skillLevels = response.getEntity(new GenericType<List<SkillLevel>>() {
		});
		Assert.assertNotNull(skillLevels, "skillLevels");
		Assert.assertFalse(skillLevels.isEmpty(), "skillLevels.isEmpty()");
		boolean found = false;
		for (SkillLevel skillLevel : skillLevels) {
			Assert.assertEquals(skillLevel.getProfileId(), addedSkillLevel.getProfileId());
			if (skillLevel.getSkillId().equals(addedSkillLevel.getSkillId())
					&& skillLevel.getLevel() == addedSkillLevel.getLevel()) {
				found = true;
			}
		}
		Assert.assertTrue(found, "Added skill level not found: " + addedSkillLevel.toString());
	}
}
