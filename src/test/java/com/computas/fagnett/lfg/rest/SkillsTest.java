package com.computas.fagnett.lfg.rest;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.TestBase;
import com.computas.fagnett.lfg.model.Skill;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang.StringUtils;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.List;

/**
 * Author: Michael Gfeller
 */
@Test(groups = {"integration"})
public class SkillsTest extends TestBase {

	private WebResource webResource;
	private Skill addedSkill;

	@BeforeClass(alwaysRun = true)
	public void setupWebResource() {
		webResource = client.resource(endpointBaseUrl + Resource.SKILLS_NAME);
	}

	public void testAddSkill() throws IOException {
		Skill skill2Add = new Skill("C#");
		ClientResponse response = webResource.type(MediaType.APPLICATION_JSON).post(ClientResponse.class, skill2Add);
		Assert.assertEquals(response.getStatus(), ClientResponse.Status.CREATED.getStatusCode());
		addedSkill = response.getEntity(Skill.class);
		Assert.assertNotNull(addedSkill);
		Assert.assertEquals(addedSkill.getName(), skill2Add.getName());
		Assert.assertTrue(StringUtils.isNotBlank(addedSkill.getId()), "addedSkill.getId() must not be blank");
		String location = response.getLocation().toString();
		String expectedLocation = "http://localhost:8090/lfg/rest/skills/" + addedSkill.getId();
		Assert.assertEquals(location, expectedLocation, "Location");
		Assert.assertTrue(response.getHeaders().containsKey(Resource.CONTENT_LOCATION));
		Assert.assertEquals(response.getHeaders().get(Resource.CONTENT_LOCATION).size(), 1);
		Assert.assertEquals(response.getHeaders().get(Resource.CONTENT_LOCATION).get(0), expectedLocation, Resource.CONTENT_LOCATION);
	}

	@Test(dependsOnMethods = {"testAddSkill"})
	public void testGetSkillById() {
		WebResource getSkillResource = client.resource(endpointBaseUrl + Resource.SKILLS_NAME + "/" + addedSkill.getId());
		ClientResponse response = getSkillResource.get(ClientResponse.class);
		Assert.assertEquals(response.getStatus(), ClientResponse.Status.OK.getStatusCode());
		Skill skill = response.getEntity(Skill.class);
		Assert.assertNotNull(skill);
		Assert.assertEquals(skill.getId(), addedSkill.getId());
		Assert.assertEquals(skill.getName(), addedSkill.getName());
	}

	@Test(dependsOnMethods = {"testGetSkillById"})
	public void testGetSkills() {
		ClientResponse response = webResource.get(ClientResponse.class);
		Assert.assertEquals(response.getStatus(), ClientResponse.Status.OK.getStatusCode());
		List<Skill> skills = response.getEntity(new GenericType<List<Skill>>() {
		});
		Assert.assertNotNull(skills);
		Assert.assertFalse(skills.isEmpty());
		Skill lastSkill = null;
		boolean foundAddedSkill = false;
		for (Skill skill : skills) {
			report(skill.toString());
			// assert sort order
			if (lastSkill != null) {
				Assert.assertTrue(lastSkill.getName().compareTo(skill.getName()) <= 0, "sorting error");
			}
			if (addedSkill.getId().equals(skill.getId())) {
				foundAddedSkill = true;
			}
			lastSkill = skill;
		}
		Assert.assertTrue(foundAddedSkill, "Added skill not found: " + addedSkill.toString());
	}

}
