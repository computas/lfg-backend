package com.computas.fagnett.lfg.rest;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.TestBase;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.lang.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Map;

/**
 * Author: Michael Gfeller
 */
@Test(groups = {"integration"})
public class EnvironmentTest extends TestBase {

	private void printMap(final Map<String, String> map) {
		for (Map.Entry<String, String> entry : map.entrySet()) {
			report(String.format("%s -> %s", entry.getKey(), entry.getValue()));
		}
	}

	public void testGetEnvironment() {
		WebResource webResource = client.resource(endpointBaseUrl + Resource.ENVIRONMENT_NAME);
		ClientResponse response = webResource.get(ClientResponse.class);
		Assert.assertEquals(response.getStatus(), ClientResponse.Status.OK.getStatusCode());
		Map<String, String> info = response.getEntity(new GenericType<Map<String, String>>() {
		});
		printMap(info);
		Assert.assertTrue(info.containsKey(Environment.APPLICATION_VERSION_KEY));
		Assert.assertTrue(StringUtils.isNotBlank(info.get(Environment.APPLICATION_VERSION_KEY)));
		Assert.assertTrue(info.containsKey(Environment.MESSAGE_KEY));
		Assert.assertTrue(info.containsKey(Environment.REST_SERVER_HOSTNAME_KEY));
		Assert.assertEquals(info.get(Environment.MESSAGE_KEY), "Don't panic!");
	}
}
