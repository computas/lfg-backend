package com.computas.fagnett.lfg;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

/**
 * Author: Michael Gfeller
 */
public class TestBase {

	protected static final boolean REPORT_TO_STD_OUT = true;
	protected static final String DEFAULT_TOMCAT_PORT = "8090";

	protected String endpointBaseUrl;
	protected Client client;

	protected final void report(String message) {
		Reporter.log(message, REPORT_TO_STD_OUT);
	}

	@Parameters({"tomcat.test.port"})
	@BeforeClass(alwaysRun = true)
	public void setupEndpoint(@Optional(DEFAULT_TOMCAT_PORT) String tomcatPort) {
		endpointBaseUrl = String.format("http://localhost:%s/lfg/rest/", tomcatPort);
	}

	@BeforeClass(alwaysRun = true)
	public void initClient() throws Exception {
		ClientConfig cfg = new DefaultClientConfig();
		cfg.getClasses().add(JacksonJsonProvider.class);
		client = Client.create(cfg);
	}
}
