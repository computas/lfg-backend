package com.computas.fagnett.lfg.interceptor;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

/**
 * Author: Michael Gfeller
 */
@Interceptor
@Logging
public class LoggingInterceptor {

	private static final Logger LOGGER = Logger.getLogger(LoggingInterceptor.class.getSimpleName());
	public static final int REPEAT = 20;
	public static final String SEPARATOR = StringUtils.repeat("-", REPEAT);

	@AroundInvoke
	public Object log(InvocationContext context) throws Exception {
		final long start = System.nanoTime();
		LOGGER.info(SEPARATOR);
		LOGGER.info("Target:   " + context.getTarget());
		LOGGER.info("Method:   " + context.getMethod().getName());
		final Object[] params = context.getParameters();
		for (int i = 0; i < params.length; i++) {
			LOGGER.info("Param:    " + params[i].toString());
		}
		Object result = context.proceed();
		LOGGER.info("Result:   " + result.toString());
		LOGGER.info(String.format("Duration: %dns", System.nanoTime() - start));
		LOGGER.info(SEPARATOR);
		return result;
	}
}
