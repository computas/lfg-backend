package com.computas.fagnett.lfg.persistence.mongodb;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.mongodb.DB;
import com.mongodb.MongoClient;

import javax.enterprise.inject.Alternative;
import java.net.UnknownHostException;

/**
 * A MongoDB database connection singleton.
 * <p/>
 * Author: Rustam Mehmandarov
 */

@Alternative
public class MongoDBConnection {
	private static DB db = null;
	private static MongoDBConnection connection = new MongoDBConnection();

	private MongoDBConnection() {
		try {
			MongoClient mongodb = new MongoClient("localhost", 27017);
			db = mongodb.getDB("testdb");
		} catch (UnknownHostException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
	}

	public static DB getConnection() {
		return db;
	}
}
