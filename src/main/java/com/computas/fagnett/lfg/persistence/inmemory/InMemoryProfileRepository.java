package com.computas.fagnett.lfg.persistence.inmemory;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.common.LfgException;
import com.computas.fagnett.lfg.common.NotFoundException;
import com.computas.fagnett.lfg.model.*;
import org.apache.commons.lang.StringUtils;

import javax.annotation.ManagedBean;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import java.util.*;

/**
 * An in-memory repository for profiles.
 * <p/>
 * Author: Michael Gfeller
 */
@ManagedBean //Enable DI if not in an EE Container
@Alternative
public class InMemoryProfileRepository implements ProfileRepository {

	private final static Map<String, Profile> PROFILE_REPO = new HashMap<String, Profile>();
	private final static Map<String, SkillLevel> SKILL_LEVEL_REPO = new HashMap<String, SkillLevel>();

	final private SkillRepository skillRepository;

	//populate with som default profiles
	static {
		final Profile kari = new Profile("Kari Nordmann");
		kari.setId(nextId());
		PROFILE_REPO.put(kari.getId(), kari);
	}

	private static String nextId() {
		return UUID.randomUUID().toString().toUpperCase();
	}

	@Inject
	public InMemoryProfileRepository(SkillRepository skillRepository) {
		this.skillRepository = skillRepository;
	}

	@Override
	public String nextIdentity() {
		return nextId();
	}

	@Override
	public void save(Profile profile) {
		if (StringUtils.isBlank(profile.getId())) {
			throw new LfgException(String.format("Saving profile: id of profile '%s' cannot be blank!",
					profile.getName()));
		}
		PROFILE_REPO.put(profile.getId(), profile);
	}

	@Override
	public void save(SkillLevel skillLevel) {
		if (StringUtils.isBlank(skillLevel.getId())) {
			throw new LfgException(String.format("Saving skill level: id of skillLevel '%s' cannot be blank!",
					skillLevel.toString()));
		}
		if (StringUtils.isBlank(skillLevel.getProfileId())) {
			throw new LfgException(String.format("Saving skill level: profileId of skillLevel '%s' cannot be blank!",
					skillLevel.toString()));
		}
		if (StringUtils.isBlank(skillLevel.getSkillId())) {
			throw new LfgException(String.format("Saving skill level: skillId of skillLevel '%s' cannot be blank!",
					skillLevel.toString()));
		}
		if (profileOfId(skillLevel.getProfileId()) == null) {
			throw new NotFoundException(String.format("Saving skill level: no profile found for skillLevel '%s'!",
					skillLevel.toString()));
		}
		if (skillRepository.skillOfId(skillLevel.getSkillId()) == null) {
			throw new NotFoundException(String.format("Saving skill level: no skill found for skillLevel '%s'!",
					skillLevel.toString()));
		}
		SKILL_LEVEL_REPO.put(skillLevel.getId(), skillLevel);
	}

	@Override
	public Profile profileOfId(String id) {
		return PROFILE_REPO.get(id);
	}

	@Override
	public SortedSet<Profile> allProfiles() {
		final SortedSet<Profile> skills = new TreeSet<Profile>(new Profile.ByName());
		skills.addAll(PROFILE_REPO.values());
		return Collections.unmodifiableSortedSet(skills);
	}

	@Override
	public SortedSet<SkillLevel> skillLevelsForProfile(String profileId) {
		if (StringUtils.isBlank(profileId)) {
			throw new IllegalArgumentException("profileId cannot be blank!");
		}
		final SortedSet<SkillLevel> skillLevels = new TreeSet<SkillLevel>(new SkillLevel.ByLevel());
		for (SkillLevel skillLevel : SKILL_LEVEL_REPO.values()) {
			if (profileId.equals(skillLevel.getProfileId())) {
				skillLevels.add(skillLevel);
			}
		}
		return Collections.unmodifiableSortedSet(skillLevels);
	}
}
