package com.computas.fagnett.lfg.persistence.inmemory;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.computas.fagnett.lfg.common.LfgException;
import com.computas.fagnett.lfg.model.Skill;
import com.computas.fagnett.lfg.model.SkillRepository;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.enterprise.inject.Alternative;
import java.util.*;


/**
 * An in-memory repository for skills.
 * <p/>
 * Author: Michael Gfeller
 */
@Alternative
public class InMemorySkillRepository implements SkillRepository {

	private final static Map<String, Skill> repository = new HashMap<String, Skill>();
	private static final Logger LOGGER = Logger.getLogger(InMemorySkillRepository.class);

	//populate with som default skills
	static {
		final Skill java = new Skill(nextId(), "Java");
		final Skill maven = new Skill(nextId(), "Maven");
		final Skill net = new Skill(nextId(), ".Net");
		final Skill c = new Skill(nextId(), "C");
		final Skill cpp = new Skill(nextId(), "C++");
		repository.put(java.getId(), java);
		repository.put(maven.getId(), maven);
		repository.put(net.getId(), net);
		repository.put(c.getId(), c);
		repository.put(cpp.getId(), cpp);
	}

	private static String nextId() {
		return UUID.randomUUID().toString().toUpperCase();
	}

	@Override
	public String nextIdentity() {
		return nextId();
	}

	@Override
	public void save(Skill skill) {
		if (StringUtils.isBlank(skill.getId())) {
			throw new LfgException(String.format("Saving skill: Id of skill '%s' cannot be blank!", skill.getName()));
		}
		repository.put(skill.getId(), skill);
	}

	@Override
	public Skill skillOfId(String id) {
		return repository.get(id);
	}

	@Override
	public SortedSet<Skill> allSkills() {
		final SortedSet<Skill> skills = new TreeSet<Skill>(new Skill.ByName());
		skills.addAll(repository.values());
		LOGGER.info("Pulling all skills from inmem repository.");
		return Collections.unmodifiableSortedSet(skills);
	}
}