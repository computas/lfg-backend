package com.computas.fagnett.lfg.persistence.inmemory;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.common.LfgException;
import com.computas.fagnett.lfg.common.NotFoundException;
import com.computas.fagnett.lfg.model.*;
import org.apache.commons.lang.StringUtils;

import javax.annotation.ManagedBean;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import java.util.*;

/**
 * An in-memory repository for projects.
 * <p/>
 * Author: Rustam Mehmandarov
 */
@ManagedBean //Enable DI if not in an EE Container
@Alternative
public class InMemoryProjectRepository implements ProjectRepository {

	private final static Map<String, Project> PROJECT_REPO = new HashMap<String, Project>();
	private final static Map<String, ProjectSkill> PROJECT_SKILL_REPO = new HashMap<String, ProjectSkill>();

	final private SkillRepository skillRepository;

	//populate with som default projects
	static {
		final Project programming = new Project("Programming 101");
		programming.setId(nextId());
		PROJECT_REPO.put(programming.getId(), programming);
	}

	private static String nextId() {
		return UUID.randomUUID().toString().toUpperCase();
	}

	@Inject
	public InMemoryProjectRepository(SkillRepository skillRepository) {
		this.skillRepository = skillRepository;
	}

	@Override
	public String nextIdentity() {
		return nextId();
	}

	@Override
	public void save(Project project) {
		if (StringUtils.isBlank(project.getId())) {
			throw new LfgException(String.format("Saving project: id of project '%s' cannot be blank!",
					project.getName()));
		}
		PROJECT_REPO.put(project.getId(), project);
	}

	@Override
	public void save(ProjectSkill projectSkill) {
		if (StringUtils.isBlank(projectSkill.getId())) {
			throw new LfgException(String.format("Saving skill level: id of skillLevel '%s' cannot be blank!",
					projectSkill.toString()));
		}
		if (StringUtils.isBlank(projectSkill.getProjectId())) {
			throw new LfgException(String.format("Saving skill level: projectId of skillLevel '%s' cannot be blank!",
					projectSkill.toString()));
		}
		if (StringUtils.isBlank(projectSkill.getSkillId())) {
			throw new LfgException(String.format("Saving skill level: skillId of skillLevel '%s' cannot be blank!",
					projectSkill.toString()));
		}
		if (projectOfId(projectSkill.getProjectId()) == null) {
			throw new NotFoundException(String.format("Saving skill level: no projects found for skillLevel '%s'!",
					projectSkill.toString()));
		}
		if (skillRepository.skillOfId(projectSkill.getSkillId()) == null) {
			throw new NotFoundException(String.format("Saving skill level: no skill found for skillLevel '%s'!",
					projectSkill.toString()));
		}
		PROJECT_SKILL_REPO.put(projectSkill.getId(), projectSkill);
	}

	@Override
	public Project projectOfId(String id) {
		return PROJECT_REPO.get(id);
	}

	@Override
	public SortedSet<Project> allProjects() {
		final SortedSet<Project> skills = new TreeSet<Project>(new Project.ByName());
		skills.addAll(PROJECT_REPO.values());
		return Collections.unmodifiableSortedSet(skills);
	}

	@Override
	public SortedSet<ProjectSkill> skillsForProject(String projectId) {
		if (StringUtils.isBlank(projectId)) {
			throw new IllegalArgumentException("projectId cannot be blank!");
		}
		final SortedSet<ProjectSkill> projectSkills = new TreeSet<ProjectSkill>(new ProjectSkill.ByLevel());
		for (ProjectSkill projectSkill : PROJECT_SKILL_REPO.values()) {
			if (projectId.equals(projectSkill.getProjectId())) {
				projectSkills.add(projectSkill);
			}
		}
		return Collections.unmodifiableSortedSet(projectSkills);
	}
}
