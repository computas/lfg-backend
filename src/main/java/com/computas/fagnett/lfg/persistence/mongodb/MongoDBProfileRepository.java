package com.computas.fagnett.lfg.persistence.mongodb;
/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.computas.fagnett.lfg.common.LfgException;
import com.computas.fagnett.lfg.common.NotFoundException;
import com.computas.fagnett.lfg.model.*;
import com.mongodb.*;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

/**
 * A MongoDB repository for profiles.
 * <p/>
 * Author: Rustam Mehmandarov
 */

@Alternative
public class MongoDBProfileRepository implements ProfileRepository {
	private static final Logger LOGGER = Logger.getLogger(MongoDBProfileRepository.class);

	private static DB db;

	final private SkillRepository skillRepository;

	//TODO: Perhaps change this. I have done this merely to check how checked exceptions can be used in relation to
	// the MongoDB
	static {
		db = MongoDBConnection.getConnection();
	}

	private static String nextId() {
		return UUID.randomUUID().toString().toUpperCase();
	}

	@Override
	public String nextIdentity() {
		return nextId();
	}

	@Inject
	public MongoDBProfileRepository(SkillRepository skillRepository) {
		this.skillRepository = skillRepository;
	}

	@Override
	public void save(Profile profile) {
		if (StringUtils.isBlank(profile.getId())) {
			throw new LfgException(String.format("Saving profile: id of profile '%s' cannot be blank!",
					profile.getName()));
		}

		DBCollection coll = db.getCollection("Profiles");
		BasicDBObject dbProfile = new BasicDBObject("id", profile.getId());
		dbProfile.append("name", profile.getName());
		dbProfile.append("description", profile.getDescription());
		dbProfile.append("programme", profile.getProgramme());
		coll.insert(dbProfile);
	}

	@Override
	public void save(SkillLevel skillLevel) {
		if (StringUtils.isBlank(skillLevel.getId())) {
			throw new LfgException(String.format("Saving skill level: id of skillLevel '%s' cannot be blank!",
					skillLevel.toString()));
		}
		if (StringUtils.isBlank(skillLevel.getProfileId())) {
			throw new LfgException(String.format("Saving skill level: profileId of skillLevel '%s' cannot be blank!",
					skillLevel.toString()));
		}
		if (StringUtils.isBlank(skillLevel.getSkillId())) {
			throw new LfgException(String.format("Saving skill level: skillId of skillLevel '%s' cannot be blank!",
					skillLevel.toString()));
		}
		if (profileOfId(skillLevel.getProfileId()) == null) {
			throw new NotFoundException(String.format("Saving skill level: no profile found for skillLevel '%s'!",
					skillLevel.toString()));
		}
		if (skillRepository.skillOfId(skillLevel.getSkillId()) == null) {
			throw new NotFoundException(String.format("Saving skill level: no skill found for skillLevel '%s'!",
					skillLevel.toString()));
		}

		DBCollection coll = db.getCollection("SkillLevel"); // If a collection / bag doesn't exist it gets created by this call.
		BasicDBObject dbSkillLevel = new BasicDBObject("id", skillLevel.getId());
		dbSkillLevel.append("profileId", skillLevel.getProfileId());
		dbSkillLevel.append("skillId", skillLevel.getSkillId());
		dbSkillLevel.append("level", (Integer) skillLevel.getLevel());
		coll.insert(dbSkillLevel);
	}

	@Override
	public Profile profileOfId(String id) {
		DBCollection coll = db.getCollection("Profiles");
		BasicDBObject query = new BasicDBObject("id", id); // Creating query object with the requested id.
		DBCursor cursor = coll.find(query);
		Profile profile = null;
		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				profile = new Profile((String) obj.get("name"));
				profile.setId((String) obj.get("id"));
				profile.setDescription((String) obj.get("description"));
				profile.setProgramme((String) obj.get("programme"));
			}
		} finally {
			cursor.close();
		}
		return profile;
	}

	@Override
	public SortedSet<Profile> allProfiles() {
		final SortedSet<Profile> profiles = new TreeSet<Profile>(new Profile.ByName());

		DBCollection coll = db.getCollection("Profiles");
		DBCursor cursor = coll.find();

		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				Profile p = new Profile((String) obj.get("name"));
				p.setId((String) obj.get("id"));
				p.setDescription((String) obj.get("description"));
				p.setProgramme((String) obj.get("programme"));
				profiles.add(p);
			}
		} finally {
			cursor.close();
		}

		return Collections.unmodifiableSortedSet(profiles);
	}

	@Override
	public SortedSet<SkillLevel> skillLevelsForProfile(String profileId) {
		if (StringUtils.isBlank(profileId)) {
			throw new IllegalArgumentException("profileId cannot be blank!");
		}
		final SortedSet<SkillLevel> skillLevels = new TreeSet<SkillLevel>(new SkillLevel.ByLevel());

		DBCollection coll = db.getCollection("SkillLevel");
		final BasicDBObject query = new BasicDBObject("profileId", profileId);
		DBCursor cursor = coll.find(query);

		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				SkillLevel dbSkillLevel = new SkillLevel((String) obj.get("profileId"), (String) obj.get("skillId"), (Integer) obj.get("level"));
				dbSkillLevel.setId((String) obj.get("id"));
				skillLevels.add(dbSkillLevel);
			}
		} finally {
			cursor.close();
		}
		return Collections.unmodifiableSortedSet(skillLevels);
	}
}
