package com.computas.fagnett.lfg.persistence.mongodb;
/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.computas.fagnett.lfg.common.LfgException;
import com.computas.fagnett.lfg.model.Skill;
import com.computas.fagnett.lfg.model.SkillRepository;
import com.mongodb.*;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.enterprise.inject.Alternative;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

/**
 * A MongoDB repository for skills.
 * <p/>
 * Author: Rustam Mehmandarov
 */
@Alternative
public class MongoDBSkillRepository implements SkillRepository {
	private static final Logger LOGGER = Logger.getLogger(MongoDBSkillRepository.class);

	private static DB db = MongoDBConnection.getConnection();

	//Populate MongoDB with some skills (this should really only be necessary to do once).
	static {
		DBCollection coll = db.createCollection("Skills", null);
		BasicDBObject java = new BasicDBObject("id", nextId()).append("name", "Java");
		BasicDBObject maven = new BasicDBObject("id", nextId()).append("name", "Maven");
		BasicDBObject c = new BasicDBObject("id", nextId()).append("name", "C");
		BasicDBObject cpp = new BasicDBObject("id", nextId()).append("name", "C++");
		BasicDBObject net = new BasicDBObject("id", nextId()).append("name", ".Net");

		coll.insert(java, maven, c, cpp, net);
	}

	// Static method to populate MongoDB in static block.
	private static String nextId() {
		return UUID.randomUUID().toString().toUpperCase();
	}

	@Override
	public String nextIdentity() {
		return nextId();
	}

	@Override
	public void save(Skill skill) {
		if (StringUtils.isBlank(skill.getId())) {
			throw new LfgException(String.format("Saving skill: Id of skill '%s' cannot be blank!", skill.getName()));
		}
		DBCollection coll = db.getCollection("Skills");
		BasicDBObject dbSkill = new BasicDBObject("id", skill.getId()).append("name", skill.getName());
		coll.insert(dbSkill);
	}

	@Override
	public Skill skillOfId(String id) {
		DBCollection coll = db.getCollection("Skills");
		BasicDBObject query = new BasicDBObject("id", id); // Creating query object with the requested id.
		DBCursor cursor = coll.find(query);
		Skill skill = null;
		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				skill = new Skill((String) obj.get("id"), (String) obj.get("name"));
			}
		} finally {
			cursor.close();
		}
		return skill;
	}

	@Override
	public SortedSet<Skill> allSkills() {
		final SortedSet<Skill> skills = new TreeSet<Skill>(new Skill.ByName());

		DBCollection coll = db.getCollection("Skills");
		DBCursor cursor = coll.find();

		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				skills.add(new Skill((String) obj.get("id"), (String) obj.get("name"))); // Creating and adding a new Skill-object from the NoSQL-document.
			}
		} finally {
			cursor.close();
		}
		LOGGER.info("Pulling all skills from MongoDB repository.");
		return Collections.unmodifiableSortedSet(skills);
	}
}
