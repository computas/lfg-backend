package com.computas.fagnett.lfg.persistence.mongodb;
/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.computas.fagnett.lfg.common.LfgException;
import com.computas.fagnett.lfg.common.NotFoundException;
import com.computas.fagnett.lfg.model.*;
import com.mongodb.*;
import org.apache.commons.lang.StringUtils;

import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import java.util.Collections;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

/**
 * A MongoDB repository for projects.
 * <p/>
 * Author: Rustam Mehmandarov
 */

@Alternative
public class MongoDBProjectRepository implements ProjectRepository {

	private static DB db = MongoDBConnection.getConnection();

	final private SkillRepository skillRepository;

	//populate with som default projects
	static {
		DBCollection coll = db.createCollection("Projects", null);
		BasicDBObject programming = new BasicDBObject("id", nextId()).append("name", "Programming 101").append("description", "Introductory programming project");
		coll.insert(programming);
	}

	@Inject
	public MongoDBProjectRepository(SkillRepository skillRepository) {
		this.skillRepository = skillRepository;
	}

	private static String nextId() {
		return UUID.randomUUID().toString().toUpperCase();
	}

	@Override
	public String nextIdentity() {
		return nextId();
	}

	@Override
	public void save(Project project) {
		if (StringUtils.isBlank(project.getId())) {
			throw new LfgException(String.format("Saving project: id of project '%s' cannot be blank!",
					project.getName()));
		}

		DBCollection coll = db.getCollection("Projects");
		BasicDBObject dbProject = new BasicDBObject("id", project.getId());
		dbProject.append("name", project.getName());
		dbProject.append("description", project.getDescription());
		coll.insert(dbProject);
	}

	@Override
	public void save(ProjectSkill projectSkill) {
		if (StringUtils.isBlank(projectSkill.getId())) {
			throw new LfgException(String.format("Saving skill level: id of skillLevel '%s' cannot be blank!",
					projectSkill.toString()));
		}
		if (StringUtils.isBlank(projectSkill.getProjectId())) {
			throw new LfgException(String.format("Saving skill level: projectId of skillLevel '%s' cannot be blank!",
					projectSkill.toString()));
		}
		if (StringUtils.isBlank(projectSkill.getSkillId())) {
			throw new LfgException(String.format("Saving skill level: skillId of skillLevel '%s' cannot be blank!",
					projectSkill.toString()));
		}
		if (projectOfId(projectSkill.getProjectId()) == null) {
			throw new NotFoundException(String.format("Saving skill level: no projects found for skillLevel '%s'!",
					projectSkill.toString()));
		}
		if (skillRepository.skillOfId(projectSkill.getSkillId()) == null) {
			throw new NotFoundException(String.format("Saving skill level: no skill found for skillLevel '%s'!",
					projectSkill.toString()));
		}

		DBCollection coll = db.getCollection("ProjectSkills");
		BasicDBObject dbProjectSkill = new BasicDBObject("id", projectSkill.getId());
		dbProjectSkill.append("projectId", projectSkill.getProjectId());
		dbProjectSkill.append("skillId", projectSkill.getSkillId());
		dbProjectSkill.append("level", (Integer) projectSkill.getLevel());
		coll.insert(dbProjectSkill);
	}

	@Override
	public Project projectOfId(String id) {
		DBCollection coll = db.getCollection("Projects");
		BasicDBObject query = new BasicDBObject("id", id);

		DBObject obj = coll.findOne(query);
		if (obj == null) // I assume obj == null if one tries to find one that does not exist
			return null;

		Project p = new Project();
		p.setId((String) obj.get("id"));
		p.setName((String) obj.get("name"));
		p.setDescription((String) obj.get("description"));

		return p;
	}

	@Override
	public SortedSet<Project> allProjects() {
		final SortedSet<Project> projects = new TreeSet<Project>(new Project.ByName());

		DBCollection coll = db.getCollection("Projects");
		DBCursor cursor = coll.find();
		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				Project p = new Project((String) obj.get("name"));
				p.setId((String) obj.get("id"));
				p.setDescription((String) obj.get("description"));
				projects.add(p);
			}
		} finally {
			cursor.close();
		}
		return Collections.unmodifiableSortedSet(projects);
	}

	@Override
	public SortedSet<ProjectSkill> skillsForProject(String projectId) {
		if (StringUtils.isBlank(projectId)) {
			throw new IllegalArgumentException("projectId cannot be blank!");
		}

		final SortedSet<ProjectSkill> projectSkills = new TreeSet<ProjectSkill>(new ProjectSkill.ByLevel());

		DBCollection coll = db.getCollection("ProjectSkills");
		BasicDBObject query = new BasicDBObject("projectId", projectId);

		DBCursor cursor = coll.find(query);

		try {
			while (cursor.hasNext()) {
				DBObject obj = cursor.next();
				ProjectSkill dbProjectSkill = new ProjectSkill();
				dbProjectSkill.setId((String) obj.get("id"));
				dbProjectSkill.setProjectId((String) obj.get("projectId"));
				dbProjectSkill.setSkillId((String) obj.get("skillId"));
				dbProjectSkill.setLevel((Integer) obj.get("level"));
				projectSkills.add(dbProjectSkill);
			}
		} finally {
			cursor.close();
		}

		return Collections.unmodifiableSortedSet(projectSkills);
	}
}
