package com.computas.fagnett.lfg.persistence.producer;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.persistence.inmemory.InMemoryProjectRepository;
import com.computas.fagnett.lfg.persistence.mongodb.MongoDBProjectRepository;
import com.computas.fagnett.lfg.model.ProjectRepository;
import com.computas.fagnett.lfg.model.SkillRepository;
import com.computas.fagnett.lfg.service.ConfigurationService;
import org.apache.log4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;

/**
 * Author: Rustam Mehmandarov
 */
public class ProjectRepositoryProducer {

	private static final Logger LOGGER = Logger.getLogger(ProjectRepositoryProducer.class);

	@Inject
	private SkillRepository skillRepository;

	@Produces
	@ApplicationScoped
	public ProjectRepository getProjectRepository(ConfigurationService configurationService) {
		LOGGER.debug("producing projects repository, using skillRepository " + skillRepository.toString());

		String dataSource = configurationService.getApplicationDataSource();

		if (dataSource.equals(DataSource.MONGO_DB)) {
			return new MongoDBProjectRepository(skillRepository);
		}

		// Should always default to inmem if not specified otherwise.
		return new InMemoryProjectRepository(skillRepository);
	}
}
