package com.computas.fagnett.lfg.common;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 - 2014 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.util.Date;

/**
 * Author: Michael Gfeller.
 */
public class Notification implements Serializable {
	private final String message;
	private final Date date;

	public Notification(String message) {
		this.message = message;
		this.date = new Date();
	}

	public String getMessage() {
		return message;
	}

	public Date getDate() {
		return date;
	}

	@Override
	public String toString() {
		return "Notification{" +
				"message='" + message + '\'' +
				", date=" + date +
				'}';
	}
}
