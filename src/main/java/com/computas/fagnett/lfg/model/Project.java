package com.computas.fagnett.lfg.model;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Comparator;

/**
 * A representation of a specific project.
 * <p/>
 * <p/>
 * Author: Rustam Mehmandarov
 */
public class Project {

	public Project(String name) {
		this.name = name;
	}

	public static class ByName implements Comparator<Project> {
		@Override
		public int compare(Project p1, Project p2) {
			return p1.name.compareTo(p2.name);
		}
	}

	private String id;
	private String name;
	private String description;
	//members, skills

	public Project() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
