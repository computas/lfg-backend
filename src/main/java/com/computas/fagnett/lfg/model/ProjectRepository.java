package com.computas.fagnett.lfg.model;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.SortedSet;

/**
 * Author: Rustam Mehmandarov
 */
public interface ProjectRepository {
	/**
	 * Returns the next identity to be used for a new entity managed by this repository.
	 *
	 * @return the next identity
	 */
	String nextIdentity();

	/**
	 * Add or update a project.
	 *
	 * @param project to add or update
	 */
	void save(Project project);

	/**
	 * Adds or updates a required skill for a project.
	 * Project skills cannot exist without projects, therefore they are managed by the projects repository.
	 * This method should also cast a NotFoundException if neither of the association ends exist.
	 *
	 * @param projectSkill the new required skill
	 */
	void save(ProjectSkill projectSkill);

	/**
	 * Retrieves a project given its id.
	 *
	 * @param id an id
	 * @return project found, or null
	 */
	Project projectOfId(String id);

	/**
	 * Retrieves all projects, sorted using the default comparator.
	 *
	 * @return all projects
	 */
	SortedSet<Project> allProjects();

	/**
	 * Finds all skill levels for a given project, or an empty set if none are found.
	 *
	 * @param projectId the id of the interesting project
	 * @return a sorted set with skill levels
	 */
	SortedSet<ProjectSkill> skillsForProject(String projectId);
}
