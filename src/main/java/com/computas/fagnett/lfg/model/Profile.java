package com.computas.fagnett.lfg.model;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Comparator;

/**
 * A representation of a profile of a person.
 * <p/>
 * Author: Michael Gfeller
 */
public class Profile {

	public Profile(String name) {
		this.name = name;
	}

	public static class ByName implements Comparator<Profile> {
		@Override
		public int compare(Profile o1, Profile o2) {
			return o1.name.compareTo(o2.name);
		}
	}

	private String id;
	private String name;
	private String description;
	private String programme;

	public Profile() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getProgramme() {
		return programme;
	}

	public void setProgramme(String programme) {
		this.programme = programme;
	}

	@Override
	public String toString() {
		return "Profile{" +
				"id='" + id + '\'' +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", programme='" + programme + '\'' +
				'}';
	}
}
