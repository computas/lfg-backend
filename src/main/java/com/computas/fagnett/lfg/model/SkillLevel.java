package com.computas.fagnett.lfg.model;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Comparator;

/**
 * SkillLevel link profiles with skills.
 * Skill levels represent the many-to-many relationship between profiles and skills.
 * Using a separate class for linking allows for later extension, e.g. when or how a specific
 * level was achieved.
 * <p/>
 * See also: http://stackoverflow.com/questions/6324547/how-to-handle-many-to-many-relationships-in-a-restful-api
 * <p/>
 * <p/>
 * Author: Michael Gfeller
 */
public class SkillLevel {

	public static class ByLevel implements Comparator<SkillLevel> {
		@Override
		public int compare(SkillLevel o1, SkillLevel o2) {
			return o1.level - o2.level;
		}
	}

	private String id;
	private String profileId;
	private String skillId;
	private int level;


	public SkillLevel() {
	}

	public SkillLevel(String profileId, String skillId, int level) {
		this.profileId = profileId;
		this.skillId = skillId;
		this.level = level;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getSkillId() {
		return skillId;
	}

	public void setSkillId(String skillId) {
		this.skillId = skillId;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	@Override
	public String toString() {
		return "SkillLevel{" +
				"id='" + id + '\'' +
				", profileId='" + profileId + '\'' +
				", skillId='" + skillId + '\'' +
				", level=" + level +
				'}';
	}
}
