package com.computas.fagnett.lfg.model;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.SortedSet;

/**
 * Author: Michael Gfeller
 */
public interface ProfileRepository {
	/**
	 * Returns the next identity to be used for a new entity managed by this repository.
	 *
	 * @return the next identity
	 */
	String nextIdentity();

	/**
	 * Add or update a profile.
	 *
	 * @param profile to add or update
	 */
	void save(Profile profile);

	/**
	 * Adds or updates a skill level.
	 * Skill levels cannot exist without profiles, therefore they are managed by the profiles repository.
	 * This method should also cast a NotFoundException if neither of the association ends exist.
	 *
	 * @param skillLevel the new skill level
	 */
	void save(SkillLevel skillLevel);

	/**
	 * Retrieves a profile given its id.
	 *
	 * @param id an id
	 * @return profile found, or null
	 */
	Profile profileOfId(String id);

	/**
	 * Retrieves all profiles, sorted using the default comparator.
	 *
	 * @return all profiles
	 */
	SortedSet<Profile> allProfiles();

	/**
	 * Finds all skill levels for a given profile, or an empty set if none are found.
	 *
	 * @param profileId the id of the interesting profile
	 * @return a sorted set with skill levels
	 */
	SortedSet<SkillLevel> skillLevelsForProfile(String profileId);
}
