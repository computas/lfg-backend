package com.computas.fagnett.lfg.model;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.SortedSet;

/**
 * Author: Michael Gfeller
 */
public interface SkillRepository {

	/**
	 * Returns the next identity to be used for a new skill.
	 *
	 * @return the next identity
	 */
	String nextIdentity();

	/**
	 * Add or update a skill.
	 *
	 * @param skill to add or update
	 */
	void save(Skill skill);

	/**
	 * Retrieves a skill given its id.
	 *
	 * @param id an id
	 * @return skill found, or null
	 */
	Skill skillOfId(String id);

	/**
	 * Retrieves all skills, sorted using the default comparator.
	 *
	 * @return all skills
	 */
	SortedSet<Skill> allSkills();

}
