package com.computas.fagnett.lfg.rest;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.common.LfgException;
import com.computas.fagnett.lfg.interceptor.Logging;
import com.computas.fagnett.lfg.model.ProfileRepository;
import com.computas.fagnett.lfg.model.SkillLevel;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Author: Michael Gfeller
 */
@Path(Resource.SKILL_LEVELS)
@ManagedBean //Enable DI if not in an EE Container
@Logging
public class SkillLevels extends Resource {
	final private ProfileRepository profileRepository;

	@Inject
	public SkillLevels(ProfileRepository profileRepository) {
		this.profileRepository = profileRepository;
	}

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response addSkillLevel(SkillLevel skillLevel) {
		skillLevel.setId(profileRepository.nextIdentity());
		profileRepository.save(skillLevel);
		final URI location = buildUri(SKILL_LEVELS, skillLevel.getId());
		return Response.created(location)
				.header(CONTENT_LOCATION, location.toString())
				.entity(skillLevel)
				.build();
	}

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<SkillLevel> getSkillLevels(@QueryParam("profile") String profileId) {
		if (StringUtils.isBlank(profileId)) {
			throw new LfgException("getSkillLevels: profileId cannot be blank!");
		}
		final List<SkillLevel> skillLevels = new ArrayList<SkillLevel>();
		skillLevels.addAll(profileRepository.skillLevelsForProfile(profileId));
		return Collections.unmodifiableList(skillLevels);
	}
}
