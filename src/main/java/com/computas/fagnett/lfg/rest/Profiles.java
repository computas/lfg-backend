package com.computas.fagnett.lfg.rest;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.common.NotFoundException;
import com.computas.fagnett.lfg.interceptor.Logging;
import com.computas.fagnett.lfg.model.Profile;
import com.computas.fagnett.lfg.model.ProfileRepository;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Author: Michael Gfeller
 */
@Path(Resource.PROFILES)
@ManagedBean //Enable DI if not in an EE Container
@Logging
public class Profiles extends Resource {

	final private ProfileRepository profileRepository;

	@Inject
	public Profiles(ProfileRepository profileRepository) {
		this.profileRepository = profileRepository;
	}


	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<Profile> getProfiles() {
		final List<Profile> allProfiles = new ArrayList<Profile>();
		allProfiles.addAll(profileRepository.allProfiles());
		return Collections.unmodifiableList(allProfiles);
	}

	@Path(ID_PARAM_PATH)
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Profile getProfile(@PathParam(ID_PARAM) String id) {
		final Profile profile = profileRepository.profileOfId(id);
		if (profile == null) {
			throw new NotFoundException(String.format("Profile with id '%s' does not exist.", id));
		}
		return profile;
	}

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response addProfile(Profile profile) {
		profile.setId(profileRepository.nextIdentity());
		profileRepository.save(profile);
		final URI location = buildUri(PROFILES, profile.getId());
		return Response.created(location)
				.header(CONTENT_LOCATION, location.toString())
				.entity(profile)
				.build();
	}
}
