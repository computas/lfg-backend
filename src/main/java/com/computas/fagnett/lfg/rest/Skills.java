package com.computas.fagnett.lfg.rest;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.common.NotFoundException;
import com.computas.fagnett.lfg.common.Notification;
import com.computas.fagnett.lfg.interceptor.Logging;
import com.computas.fagnett.lfg.model.Skill;
import com.computas.fagnett.lfg.model.SkillRepository;
import org.apache.log4j.Logger;

import javax.annotation.ManagedBean;
import javax.enterprise.event.Event;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Author: Michael Gfeller
 */
@Path(Resource.SKILLS)
@ManagedBean //Enable DI if not in an EE Container
@Logging
public class Skills extends Resource {
	final private SkillRepository skillRepository;

	@Inject @Any
	Event<Notification> notificationEvent;

	@Inject
	public Skills(SkillRepository skillRepository) { // This is called after all. However, it is important that the annotation used here match the ones used in the declaration of skillRepository
		this.skillRepository = skillRepository;
	}

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<Skill> getSkills() {
		final List<Skill> allSkills = new ArrayList<Skill>();
		allSkills.addAll(skillRepository.allSkills());
		return Collections.unmodifiableList(allSkills);
	}

	@Path(ID_PARAM_PATH)
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Skill getSkill(@PathParam(ID_PARAM) String id) {
		final Skill skill = skillRepository.skillOfId(id);
		if (skill == null) {
			throw new NotFoundException(String.format("Skill with id '%s' does not exist.", id));
		}
		return skill;
	}

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response addSkill(Skill skill) {
		skill.setId(skillRepository.nextIdentity());
		skillRepository.save(skill);
		final URI location = buildUri(SKILLS, skill.getId());
		notificationEvent.fire(new Notification("Adding new skill: " + skill));
		return Response.created(location)
				.header(CONTENT_LOCATION, location.toString())
				.entity(skill)
				.build();
	}
}
