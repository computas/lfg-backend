package com.computas.fagnett.lfg.rest;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.service.ConfigurationService;
import org.apache.commons.lang.StringUtils;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Author: Michael Gfeller
 */
@ManagedBean //Enable DI if not in an EE Container
public abstract class Resource {

	static final String ENVIRONMENT_NAME = "environment";
	static final String ENVIRONMENT = "/" + ENVIRONMENT_NAME;
	static final String SKILLS_NAME = "skills";
	static final String SKILLS = "/" + SKILLS_NAME;
	static final String ID_PARAM = "id";
	static final String ID_PARAM_PATH = "/{" + ID_PARAM + "}";
	static final String CONTENT_LOCATION = "Content-Location";
	static final String PROFILES_NAME = "profiles";
	static final String PROFILES = "/" + PROFILES_NAME;
	static final String SKILL_LEVELS_NAME = "skilllevels";
	static final String SKILL_LEVELS = "/" + SKILL_LEVELS_NAME;
	static final String PROJECTS_NAME = "projects";
	static final String PROJECTS = "/" + PROJECTS_NAME;
	static final String PROJECT_SKILLS_NAME = "projectskills";
	static final String PROJECT_SKILLS = "/" + PROJECT_SKILLS_NAME;

	@Inject
	ConfigurationService configurationService;

	/**
	 * Appends all segments to the root url, removing leading '/'-character from segment
	 *
	 * @param segments path segments
	 * @return the URI
	 */
	protected URI buildUri(String... segments) {
		String path = configurationService.getRootUrl();
		for (String segment : segments) {
			String noLeadingSlash = StringUtils.removeStart(segment, "/");
			path = path.endsWith("/") ? path + noLeadingSlash : path + "/" + noLeadingSlash;
		}
		return UriBuilder.fromPath(path).build();
	}

}
