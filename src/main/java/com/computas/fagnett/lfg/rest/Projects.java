package com.computas.fagnett.lfg.rest;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.computas.fagnett.lfg.common.NotFoundException;
import com.computas.fagnett.lfg.interceptor.Logging;
import com.computas.fagnett.lfg.model.Project;
import com.computas.fagnett.lfg.model.ProjectRepository;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Author: Rustam Mehmandarov
 */
@Path(Resource.PROJECTS)
@ManagedBean //Enable DI if not in an EE Container
@Logging
public class Projects extends Resource {

	final private ProjectRepository projectRepository;


	@Inject
	public Projects(ProjectRepository projectRepository) {
		this.projectRepository = projectRepository;
	}

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<Project> getProjects() {
		final List<Project> allProjects = new ArrayList<Project>();
		allProjects.addAll(projectRepository.allProjects());
		return Collections.unmodifiableList(allProjects);
	}

	@Path(ID_PARAM_PATH)
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Project getProject(@PathParam(ID_PARAM) String id) {
		final Project project = projectRepository.projectOfId(id);
		if (project == null) {
			throw new NotFoundException(String.format("Project with id '%s' does not exist.", id));
		}
		return project;
	}

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response addProject(Project project) {
		project.setId(projectRepository.nextIdentity());
		projectRepository.save(project);
		final URI location = buildUri(PROJECTS, project.getId());
		return Response.created(location)
				.header(CONTENT_LOCATION, location.toString())
				.entity(project)
				.build();
	}
}
