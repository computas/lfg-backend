package com.computas.fagnett.lfg.rest;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.computas.fagnett.lfg.common.LfgException;
import com.computas.fagnett.lfg.interceptor.Logging;
import com.computas.fagnett.lfg.model.ProjectRepository;
import com.computas.fagnett.lfg.model.ProjectSkill;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: dpd
 * Date: 15.11.13
 * Time: 13:25
 * To change this template use File | Settings | File Templates.
 *
 * @author Daniel.Peder.Dahlsveen@computas.com
 * @since 13:25 15.11.13
 */
@Path(Resource.PROJECT_SKILLS)
@ManagedBean
@Logging
public class ProjectSkills extends Resource {
	final private ProjectRepository projectRepository;

	@Inject
	public ProjectSkills(ProjectRepository projectRepository) {
		this.projectRepository = projectRepository;
	}

	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public Response addProjectSkill(ProjectSkill projectSkill) {
		projectSkill.setId(projectRepository.nextIdentity());
		projectRepository.save(projectSkill);
		final URI location = buildUri(PROJECT_SKILLS, projectSkill.getId());
		return Response.created(location)
				.header(CONTENT_LOCATION, location.toString())
				.entity(projectSkill)
				.build();
	}

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<ProjectSkill> getProjectSkills(@QueryParam("project") String projectId) {
		if (StringUtils.isBlank(projectId)) {
			throw new LfgException("getProjectSkills: projectid cannot be blank!");
		}
		final List<ProjectSkill> projectSkills = new ArrayList<ProjectSkill>();
		projectSkills.addAll(projectRepository.skillsForProject(projectId));
		return Collections.unmodifiableList(projectSkills);
	}
}
