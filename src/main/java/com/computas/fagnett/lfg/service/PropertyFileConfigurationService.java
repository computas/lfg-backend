package com.computas.fagnett.lfg.service;

/*
 * #%L
 * lfg-backend
 * %%
 * Copyright (C) 2013 Computas AS
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.log4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Author: Michael Gfeller
 */
@ApplicationScoped
public class PropertyFileConfigurationService implements ConfigurationService {

	private static final Logger LOGGER = Logger.getLogger(PropertyFileConfigurationService.class);
	private static final String PROPERTIES_FILENAME = "lfgrest.properties";

	private static final String LFG_PROPERTIES_FILEPATH_ENV_VAR_NAME = "lfgrest.properties.filepath";
	private static final String LFG_PROPERTIES_FILEPATH_PROPERTY_NAME = LFG_PROPERTIES_FILEPATH_ENV_VAR_NAME;
	// e.g. JVM property: -Dlfgrest.properties.filepath=/home/mgf/dev/lfg/lfg-backend/lfgrest.properties

	private static final String APPLICATION_VERSION_PROPERTY = "lfg.application.version";
	private static final String APPLICATION_DATASOURCE_PROPERTY = "lfg.application.dataSource";

	private static final String ROOT_URL_PROPERTY = "lfg.root.url";
	private static final String LFG_WELCOME_MESSAGE_PROPERTY = "lfg.welcome.message";

	private final Properties properties = new Properties();

	public PropertyFileConfigurationService() {
		try {
			String filePath = System.getProperty(LFG_PROPERTIES_FILEPATH_PROPERTY_NAME, System.getenv(LFG_PROPERTIES_FILEPATH_ENV_VAR_NAME));
			File file = (filePath == null ? null : new File(filePath));
			boolean useFile = (file != null) && file.exists() && file.isFile();
			if (useFile) {
				LOGGER.info(String.format("Using property file from filesystem: '%s'", filePath));
				properties.load(new FileInputStream(file));
			} else {
				LOGGER.info(String.format("Using property file resource: '%s'", PROPERTIES_FILENAME));
				properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(PROPERTIES_FILENAME));
			}
		} catch (Exception e) {
			LOGGER.warn(String.format("Error reading property file (%s), use default values.", e.getMessage()));
		}
	}

	@Override
	public String getProperty(String propertyName, String defaultValue) {
		return properties.getProperty(propertyName, defaultValue);
	}

	@Override
	public String getApplicationVersion() {
		return properties.getProperty(APPLICATION_VERSION_PROPERTY, "");
	}

	@Override
	public String getApplicationDataSource() {
		return properties.getProperty(APPLICATION_DATASOURCE_PROPERTY, "");
	}

	@Override
	public String getWelcomeMessage() {
		return properties.getProperty(LFG_WELCOME_MESSAGE_PROPERTY, "Hello Seeker!");
	}

	@Override
	public String getRootUrl() {
		return properties.getProperty(ROOT_URL_PROPERTY, "");
	}
}
